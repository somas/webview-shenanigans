# window.py
#
# Copyright 2022 Manuel Genovés
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later
import gi
gi.require_version('WebKit2', '5.0')
from gi.repository import Adw, Gtk, WebKit2

@Gtk.Template(resource_path='/es/escapist/webview_shenanigans/window.ui')
class WebviewShenanigansWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'WebviewShenanigansWindow'

    add_button = Gtk.Template.Child()
    stack = Gtk.Template.Child()
    flap = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @Gtk.Template.Callback()
    def add_webview(self, _widget, _data=None):
        webview = WebKit2.WebView.new()
        webview.load_uri("https://www.google.es")
        webview.show()
        self.stack.add_child(webview)
        self.stack.set_visible_child(webview)
        self.flap.set_reveal_flap(True)
